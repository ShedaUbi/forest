using UnityEngine;
using System.Collections;

public class TerrainGenerator : MonoBehaviour 
{
	public GameObject tree;
	public float R=0.25f; // Коэффициент скалистости
	public int GRAIN=2; // Коэффициент зернистости
	public bool FLAT = false; // Делать ли равнины
	public Material material; 
	public GameObject rotator;

	private int width=2048;
	private int height=2048;
	private float WH;
	private Color32[] cols;
	private Texture2D texture;
	private float[,] heights;
	private Vector2[] treePoses;

	
	void Start () 
	{
		GenerateTerrain();
		SpawnTrees();
		ChangeCameraPos();
	}

	private void GenerateTerrain()
	{
		int resolution = width;
		WH = (float)width+height;

		// Задаём карту высот
		Terrain terrain = FindObjectOfType<Terrain> ();
		heights = new float[resolution,resolution]; 

		// Создаём карту высот
		texture = new Texture2D(width, height);
		cols = new Color32[width*height];
		drawPlasma(width, height);
		texture.SetPixels32(cols);
		texture.Apply();

		// Используем шейдер (смотри пункт 3 во 2 части)
		material.SetTexture ("_HeightTex", texture);

		// Задаём высоту вершинам по карте высот
		for (int i=0; i<resolution; i++) {
			for (int k=0;k<resolution; k++){
				heights[i,k] = texture.GetPixel(i,k).grayscale*R;
				heights[i,k] = Mathf.Round(heights[i,k] / 0.0025f) * 0.0025f;
			}
		}

		// Применяем изменения
		terrain.terrainData.size = new Vector3(width, width, height);
		terrain.terrainData.heightmapResolution = resolution;
		terrain.terrainData.SetHeights(0, 0, heights);
	}

    // Считаем рандомный коэффициент смещения для высоты
	float displace(float num)
	{
		float max = num / WH * GRAIN;
		return Random.Range(-0.5f, 0.5f)* max;
	}
	
        // Вызов функции отрисовки с параметрами
	void drawPlasma(float w, float h) 
	{
		float c1, c2, c3, c4;
		
		c1 = Random.value;
		c2 = Random.value;
		c3 = Random.value;
		c4 = Random.value;
		
		divide(0.0f, 0.0f, w , h , c1, c2, c3, c4);
	}
	
        // Сама рекурсивная функция отрисовки
	void divide(float x, float y, float w, float h, float c1, float c2, float c3, float c4)
	{
		
		float newWidth = w * 0.5f;
		float newHeight = h * 0.5f;
		
		if (w < 1.0f && h < 1.0f)
		{
			float c = (c1 + c2 + c3 + c4) * 0.25f;
			cols[(int)x+(int)y*width] = new Color(c, c, c);
		}
		else
		{
			float middle =(c1 + c2 + c3 + c4) * 0.25f + displace(newWidth + newHeight);
			float edge1 = (c1 + c2) * 0.5f;
			float edge2 = (c2 + c3) * 0.5f;
			float edge3 = (c3 + c4) * 0.5f;
			float edge4 = (c4 + c1) * 0.5f;

			if(!FLAT){
				if (middle <= 0)
				{
					middle = 0;
				}
			else if (middle > 1.0f)
				{
					middle = 1.0f;
				}
			}
			divide(x, y, newWidth, newHeight, c1, edge1, middle, edge4);
			divide(x + newWidth, y, newWidth, newHeight, edge1, c2, edge2, middle);
			divide(x + newWidth, y + newHeight, newWidth, newHeight, middle, edge2, c3, edge3);
			divide(x, y + newHeight, newWidth, newHeight, edge4, middle, edge3, c4);
		}
	}

	private void SpawnTrees()
	{	
		//for(int j = 0; j < n; j++)
		//{		
			FindTreePoses();
			
		Vector3 terrainPos = transform.position;

		for (int i = 0; i < treePoses.Length; i++) // кол-во ячеек в treePoses
        {
			
			//float RangeTree = Random.Range(0, 100); //расстояние радиуса деревьев
			//float a = Random.Range(0, 360); //рандомный вектор

			if(treePoses[i] == Vector2.zero)
				continue;

			int treeX = (int)(treePoses[i].x); // x iго элемента treePoses
			int treeZ = (int)(treePoses[i].y); // y iго элемента treePoses
            Instantiate(tree, terrainPos + new Vector3(treeX, heights[treeZ, treeX] * 2048f, treeZ), Quaternion.Euler(0f, Random.Range(-50f, 50f), 0f));
        }
		//}
		
	}

	private void FindTreePoses()
	{	
		int n = 15;
		int m = 20;
		int g = 15;
		
		bool badPos = false;

		treePoses = new Vector2[n * m * g];
		int count = 0;

		Vector2[] tempPoses = new Vector2[n];
		Vector2[] tempPoses2 = new Vector2[m];

		int badCounter1 = 0;

		for(int i = 0; i < n; i++)
		{		
			tempPoses[i] = (i == 0 ? 
				new Vector2((int)Random.Range(width * 0.45f, width * 0.55f), (int)Random.Range(height * 0.45f, height * 0.55f)) : 
				new Vector2((int)Random.Range(width * 0.2f, width * 0.8f), (int)Random.Range(height * 0.2f, height * 0.8f))
			);
			
			if (i != 0)
			{
				badPos = false;
				for(int j = 0; j < i; j++)
				{
					if (Vector2.Distance(tempPoses[i], tempPoses[j]) < 300f)
					{
						badPos = true;
						break;
					}
				}

				if(badPos)
				{
					Debug.Log("Bad pos 1");

					badCounter1++;

					if(badCounter1 > 5)
						break;

					i--;
					continue;
				}
			}

			int badCounter2 = 0;

			for(int k = 0; k < m; k++)
			{
				tempPoses2[k] = tempPoses[i] + new Vector2((int)Random.Range(-200f, 200f), (int)Random.Range(-200f, 200f));

				if (k != 0)
				{
					badPos = false;
					for(int j = 0; j < k; j++)
					{
						if (Vector2.Distance(tempPoses2[k], tempPoses2[j]) < 40f)
						{
							badPos = true;
							break;
						}
					}

					if(badPos)
					{
						Debug.Log("Bad pos 2");

						badCounter2++;

						if(badCounter2 > 5)
							break;

						k--;
						continue;
					}
				}

				int badCounter3 = 0;

				for(int l = 0; l < g; l++)
				{
					treePoses[count] = tempPoses2[k] + new Vector2((int)Random.Range(-40f, 40f), (int)Random.Range(-40f, 40f));
					count++;

					if (l != 0)
					{
						badPos = false;
						for(int j = 0; j < count - 1; j++)
						{
							if (Vector2.Distance(treePoses[count - 1], treePoses[j]) < 2f)
							{
								badPos = true;
								break;
							}
						}

						if(badPos)
						{
							Debug.Log("Bad pos 3");

							badCounter3++;

							if(badCounter3 > 5)
								break;

							l--;
							count--;

							continue;
						}
					}
				}
			}
		}
	}

	private void ChangeCameraPos()
	{
		rotator.transform.position = new Vector3(0f, heights[(int)(height * 0.5), (int)(width * 0.5)] * 2048f + 60f, 0f);
		// 0f, heights[height * 0.5f, width * 0.5f] * 2048f, 0f
	}
}