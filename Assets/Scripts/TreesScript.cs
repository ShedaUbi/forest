using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreesScript : MonoBehaviour
{
    private void Start()    
    {
        int i = Random.Range(0, 100);
        Transform child = transform.GetChild(i>50?0:1);
        Destroy(child.gameObject);

        GetComponent<Transform>().localScale = new Vector3(1f, Random.Range(0.8f, 1.2f), 1f);
    }
}
